//
//  Movie.swift
//  assign
//
//  Created by Anurag vij on 27/01/23.
//

import Foundation
import UIKit

struct Movie {
    let title: String
    let type: String
    let image: UIImage
}

let movies: [Movie] = [
    Movie(title: "Hightown",type:"Horror", image: #imageLiteral(resourceName: "hightown.png")),
    Movie(title: "Penny Dreadful",type:"Comedy", image: #imageLiteral(resourceName: "pennyDreadful")),
    Movie(title: "The Bold Type",type:"Action", image: #imageLiteral(resourceName: "boldType")),
    Movie(title: "Blindspot",type:"Horror", image: #imageLiteral(resourceName: "blindspot")),
    Movie(title: "In the Dark",type:"Drama", image: #imageLiteral(resourceName: "inTheDark")),
    Movie(title: "Doom Patrol",type:"Horror", image: #imageLiteral(resourceName: "doomPatrol.png")),
    Movie(title: "Agents of S.H.I.E.L.D",type:"Comedy", image: #imageLiteral(resourceName: "agentsOfShield.png")),
    Movie(title: "The 100",type:"Horror", image: #imageLiteral(resourceName: "theHundred")),
    Movie(title: "DC Legends of Tomorrow",type:"Horror", image: #imageLiteral(resourceName: "dcLegendsOfTomorrow")),
    Movie(title: "Siren",type:"Horror", image: #imageLiteral(resourceName: "siren"))
]
