//
//  MovieCollectionViewCell.swift
//  assign
//
//  Created by Anurag vij on 27/01/23.
//

import UIKit

class MovieCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var movieImageView: UIImageView!
    @IBOutlet weak var typeLabel: UILabel!
    
    @IBOutlet weak var titleLabel: UILabel!
    func setup(with movie: Movie){
        movieImageView.image=movie.image
        titleLabel.text=movie.title
        typeLabel.text=movie.type
    }
}
